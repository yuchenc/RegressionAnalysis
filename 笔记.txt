read.table(“文件名.后缀”)
data.txt

yx[,1]
读取yx[data]的第一列

data.frame(y,x)	
以y和x生成一个特殊的数据框

plot(y~x)
以x为横坐标、y为纵坐标做散点图

lm.sol=lm(y~x,data=mydata)
生成一个lm.sol[value]以x为自变量、y为因变量，从my data中读取数据

abline(lm.sol)
画出回归直线的图像

summary(lm.sol)	
输出回归后的数据

y.hat=predict(lm.sol)
输出y的预测值y.hat

e.hat=y-y.hat
e.hat=residuals(lm.sol)
输出残差e.hat

e.std=rstandard(lm.sol)
输出标准化残差e.std

e.stu=rstudent(lm.sol)
输出学生化残差e.stu

new=data.frame(x=15.3)
当x=15.3时对y进行预测，必须为数据框格式

y0=predict(lm.sol,new,interval="prediction",level=0.95)
利用lm.sol得到的回归分析，对new进行预测

plot(lm.sol,which=1)
residuals vs fitted得到普通残差图

plot(r.std y.fit,xlab=expression(hat(y)),ylab=“r”)
111

text(y.fit,y.rst,type=“1:25”)
type是什么鬼

boxcox(lm.sol,plotit=T,lambda=seq(-2,2,by=0.05))
library(MASS)查看boxcox变换的最好取值

vif(lm.sol)
输出每个自变量的膨胀因子大小

X=cbind(x1,x2,x3,x4,x5)
cbind是根据列进行合并,合并的前提是所有数据行数相等。rbind是根据行进行合并,就是自动往下面顺延,但要求所有数据列数是相同的才能用

rho=cor(X)
输出相关系数

eigen(rho)
输出特征值和特征向量

kappa(rho,exact=TRUE)
条件数

yxs=scale(yx)
中心标准化

rr.sol=lm.ridge(y~0+x1+x2+x3,data=mydata2,lambda=c(seq(0,0.01,by=0.001),seq(0.02,0.1,by=0.01),seq(0.2,1,by=0.1)))
岭估计

plot(rr.sol)
作岭迹图

matplot(rr.sol$lambda,t(rr.sol$coef),type="l",col=c("red","blue","black"),main="ridge trace",xlab=expression(lambda),ylab=expression(hat(beta)(lambda)))
作岭迹图

economy.pr=princomp(~x1+x2+x3,data=economy,cor=TRUE)summary(economy.pr,loadings=TRUE)	
主成分分析

pre=predict(economy.pr)
计算主成分得分（即新变量的观测值向量）	

install.packages(“”)
下载程序包

t<-sqrt((n-p-1)/(n-p-r.stu^2))*r.stu
生成t检验值
result<-data.frame(y,y.hat,e,h.value,r.stu,t)

outlierTest(lm.sol)
异常点检验，要用library(“car”)

durbinWatsonTest(lm.1)
DW检验，要用library(“car”)

confint(lm.sol,level=0.95)
回归系数的区间估计，默认level=0.95

new=data.frame(x1=1.5,x2=7.5,x3=1315) lm.pred=predict(lm.sol,new,interval=”prediction”,level=0.95)
因变量的区间预测

X=matrix(c(x1,x2,x3,x4),nrow=13,byrow=F)
library(faraway)library(leaps)
adjr=leaps(X,y,int=T,method=“adjr2”)
算每一个情况的R_adj^2
adjr$which[which.max(adjr$adjr2),]
选出最大的那个

cp=leaps(X,y,int=T,method="Cp")
算出每一个情况的C_p
cp$which[which.min(cp$"Cp"),]
选出最小的那个
Cpplot(cp)
作出图像
leaps中的method只有三种选择：method=c(“Cp”,”adjr2”,”r2”)

search.results=regsubsets(y∼x1+x2+x3+x4,data=cement,method="exhaustive",nbest=15)
selection.criteria=summary(search.results)
names(selection.criteria)
n<-length(cement[, 1])
q<-as.integer(row.names(selection.criteria$which))
R.sq<-selection.criteria$rsq
AdjR.sq<-selection.criteria$adjr2
mse<-selection.criteria$rss/(n-q-1)
Cp<-selection.criteria$cp
aic.f<-n*log(selection.criteria$rss)+2*(q+1)
bic.f<-n*log(selection.criteria$rss)+(q+1)*log(n)
var<-as.matrix(selection.criteria$which[,2:5])
criteria.table<-data.frame(cbind(q,mse,R.sq,AdjR.sq,Cp,aic.f,bic.f,var[,1],var[,2],var[,3],var[,4]),row.names=NULL)
names(criteria.table)<-c("q","MSE","Rsq","aRsq","Cp","AIC","BIC","x1","x2","x3","x4")
round(criteria.table,2)
让我一次输出个够！！！

DAAG package中的press(model)可以返回model的press值
library(DAAG)
mods<-c(“y~x1","y~x2","y~x3","y~x4","y~x1+x2","y~x1+x3","y~x1+x4","y~x2+x3","y~x2+x4","y~x3+x4","y~x1+x2+x3","y~x1+x2+x4","y~x1+x3+x4","y~x2+x3+x4","y~x1+x2+x3+x4")
P<-numeric(15)
for (i in 1:15) {mod=lm(mods[i],cement)P[i]=press(mod)}
P<-data.frame("PRESS"=P,row.names=mods)
round(P,2)

leap package中的”regsubsets”也有图示法的变量选择功能
require(leaps)
subsets<-regsubsets(y~x1+x2+x3+x4,data=cement)
summary(subsets)
plot(subsets)
plot(subsets,scale="Cp)
plot(subsets,scale="adjr2")

min.model<-lm(y~1,data=cement)
fwd.model<-step(min.model,direction="forward",scope=(~x1+x2+x3+x4))
summary(fwd.model)
向前法

full.model<-lm(y~x1+x2+x3+x4,data=cement)
bwd.model<-step(full.model,direction="backward")
summary(bwd.model)
向后法

min.model<-lm(y~1,data=cement)
step.model<-step(min.model,direction="both",scope=(~x1+x2+x3+x4))
summary(step.model)
逐步回归法

lm.sol=lm(y∼x1+x2+u1+u2,data=alcohol)
A=matrix(c(0,0,0,1,0,0,0,0,0,1), nrow=2, byrow=T)
b=c(0,0)library(car)
linearHypothesis(lm.sol,hypothesis.matrix=A,rhs=b,test=”F”)
用car package中的linearHypothesis()进行检验

x=yx[, 1]n=yx[, 2]m=yx[, 3]k=n-mhouse=data.frame(x,m,k)glm.sol=glm(cbind(m,k)∼x,family=binomial(link=”logit”),data=house)
summary(glm.sol)
logistic回归

new=data.frame(x=c(6,7,8,9))
glm.pred=predict(lm.sol,new)
pred=exp(glm.pred)/(1+exp(glm.pred))


glm.sol=glm(y∼x1+x2+x3,family=binomial(link=”logit”),data=accident) summary(glm.sol)
step.model=step(lm.reg,direction=“both”,scope=(~x1+x2+x3))
summary(step.model)
